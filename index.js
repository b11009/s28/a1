fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => { 
	const titles = json.map(todo => todo.title)
	console.log(titles)
});

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	header: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: 'Created To Do List Item',
		id: 201,
		userId:1
	}),

})
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure',
		status: 'Pending',
		dateCompleted:'Pending',
		userId:1
	})
})
.then((response)=> response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		status: 'Complete',
		dateCompleted:'07/09/21'
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE'
});






